resource "aws_ecs_cluster" "fargate" {
  name = "Fargate"
  capacity_providers = ["FARGATE_SPOT"]
  default_capacity_provider_strategy {
    capacity_provider = "FARGATE_SPOT"
  }
}

resource "aws_security_group" "server" {
  name        = "valheim-server"
  description = "Allow UDP inbound traffic"
  vpc_id      = data.aws_vpc.default.id

  ingress {
    description      = "Server Ports"
    from_port        = 2456
    to_port          = 2458
    protocol         = "udp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "valheim-server"
  }
}

resource "aws_ecs_service" "server" {
  name            = "Valheim"
  cluster         = aws_ecs_cluster.fargate.id
  task_definition = aws_ecs_task_definition.service.arn
  desired_count   = 1
  
  network_configuration {
      subnets = [data.aws_subnet.this.id]
      security_groups = [data.aws_security_group.default.id, aws_security_group.server.id]
      assign_public_ip = true
  }

  capacity_provider_strategy {
    base = 0
    capacity_provider = "FARGATE_SPOT"
    weight = 1
  }
}

resource "aws_ecs_task_definition" "service" {
  family = "service"
  network_mode = "awsvpc"
  execution_role_arn = "arn:aws:iam::${local.account_id}:role/ecsTaskExecutionRole"
  container_definitions = jsonencode([
    {
      name      = "Server"
      image     = "mhaley/valheim-server"
      essential = true
      cpu       = 0
      portMappings = [for port in [2456, 2457, 2458]: {
          containerPort = port
          hostPort      = port
          protocol      = "udp"
      }]
      readonlyRootFilesystem = false
      environment: [
            {
                "name": "BACKUPS_DIRECTORY",
                "value": "/config/backups"
            },
            {
                "name": "SERVER_PORT",
                "value": "2456"
            },
            {
                "name": "BACKUPS_FILE_PERMISSIONS",
                "value": "644"
            },
            {
                "name": "STEAMCMD_ARGS",
                "value": "validate"
            },
            {
                "name": "UPDATE_INTERVAL",
                "value": "900"
            },
            {
                "name": "WORLDS_DIRECTORY_PERMISSIONS",
                "value": "755"
            },
            {
                "name": "BACKUPS_DIRECTORY_PERMISSIONS",
                "value": "755"
            },
            {
                "name": "CONFIG_DIRECTORY_PERMISSIONS",
                "value": "755"
            },
            {
                "name": "WORLD_NAME",
                "value": var.world_name
            },
            {
                "name": "WORLDS_FILE_PERMISSIONS",
                "value": "644"
            },
            {
                "name": "SERVER_PASS",
                "value": var.server_pass
            },
            {
                "name": "DNS_1",
                "value": "8.8.8.8"
            },
            {
                "name": "SERVER_PUBLIC",
                "value": "1"
            },
            {
                "name": "SERVER_NAME",
                "value": var.server_name
            },
            {
                "name": "BACKUPS_INTERVAL",
                "value": "3600"
            },
            {
                "name": "BACKUPS_MAX_AGE",
                "value": "3"
            },
            {
                "name": "RESTART_CRON",
                "value": "''"
            }
        ],
        mountPoints: [
            {
                "sourceVolume": "Valheim-EFS",
                "containerPath": "/config"
            }
        ],
        "logConfiguration": {
            "logDriver": "awslogs",
            "options": {
                "awslogs-group": local.log_group_name,
                "awslogs-region": local.region_id,
                "awslogs-stream-prefix": "ecs"
            }
        }
    },
  ])

  volume {
    name = "Valheim-EFS"

    efs_volume_configuration {
      file_system_id          = aws_efs_file_system.backup.id
      root_directory          = "/"
      transit_encryption      = "ENABLED"
      authorization_config {
        access_point_id = aws_efs_access_point.backup.id
        iam             = "DISABLED"
      }
    }
  }

  requires_compatibilities = ["FARGATE"]
  cpu = var.cpu
  memory = var.memory
}

resource "aws_cloudwatch_log_group" "server" {
  name = local.log_group_name
  retention_in_days = 7
}
