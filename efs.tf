resource "aws_efs_file_system" "backup" {
  creation_token = var.server_name

  lifecycle_policy {
    transition_to_ia = "AFTER_30_DAYS"
  }

  availability_zone_name = data.aws_subnet.this.availability_zone
  encrypted = true
}

resource "aws_efs_access_point" "backup" {
  file_system_id = aws_efs_file_system.backup.id
}

resource "aws_efs_mount_target" "backup" {
  file_system_id = aws_efs_file_system.backup.id
  subnet_id      = data.aws_subnet.this.id
  security_groups = [data.aws_security_group.default.id]
}
