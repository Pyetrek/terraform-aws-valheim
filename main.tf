variable world_name {}
variable server_pass {}
variable server_name {}
variable az_id {}
variable cpu {
  default = 1024
}
variable memory {
  default = 3072
}

data "aws_caller_identity" "current" {}
data "aws_region" "current" {}

data "aws_vpc" "default" {
  default = true
}

data "aws_subnet" "this" {
  vpc_id = data.aws_vpc.default.id
  availability_zone = var.az_id
}

data "aws_security_group" "default" {
  vpc_id = data.aws_vpc.default.id
  name = "default"
}

locals {
    account_id = data.aws_caller_identity.current.account_id
    region_id = data.aws_region.current.name
    log_group_name = "/ecs/Valheim"
}
